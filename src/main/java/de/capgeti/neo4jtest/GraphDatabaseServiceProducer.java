package de.capgeti.neo4jtest;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

@Singleton
@Startup
public class GraphDatabaseServiceProducer {

	private GraphDatabaseService graphDatabaseService;

	@PostConstruct
	public void init() {
		graphDatabaseService = new GraphDatabaseFactory().newEmbeddedDatabase(new File("/tmp/db-test/"));
	}

	@PreDestroy
	public void remove() {
		graphDatabaseService.shutdown();
	}

	@Produces
	public GraphDatabaseService getGraphDatabaseService() {
		return graphDatabaseService;
	}
}
