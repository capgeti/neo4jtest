package de.capgeti.neo4jtest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

@Path("test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WebController {

	@Inject
	private GraphDatabaseService graphDb;

	@POST
	public Object create() {
		try (Transaction transaction = graphDb.beginTx()) {
			Node firstNode = graphDb.createNode();
			firstNode.setProperty("message", "Hello, ");
			transaction.success();
		}
		return "created";
	}

	@GET
	public Object get() {
		Result result = graphDb.execute("match (n) return n");
		return result.resultAsString();
	}
}
